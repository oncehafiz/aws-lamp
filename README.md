## Setting up an AWS EC2 Instance with LAMP and GIT
If you’re reading this then you’re probably interested in hosting a web application with Amazon’s Web Services (AWS) and taking advantage of Amazons cloud computing capabilities for your web application in this step by step tutorial we will go over the process of accomplishing this plus a few extra tips and tricks on the way.

### Connect to server with your PEM Key using SSH

```sh
$ chmod 600 path/to/yourkeyname.pem
```
then connect to your server
```sh
$ ssh –i path/to/yourkeyname.pem Ubuntu@elasticip
```
Once in and have accept the servers finger print.

### Install Linux Apache MySQL PHP (LAMP) Server

```sh
$ sudo apt-get update
```

Once Ubuntu has been updated, run the following command to install a complete LAMP stack on ubuntu, enter yes or ‘y’ when prompted followed by your MySQL password for root user twice, take note of this password for connecting you your databases later.

```sh
$ sudo apt-get install lamp-server
```


### Install PHPMyAdmin

```sh
$ sudo apt-get install phpmyadmin
```
Enter ‘y’ when prompted then select apache2 when prompted followed by no when asked to use default databases. After the installation has finished you will need to execute a few commands to allow for you to access /phpmyadmin in your browser, you will need to add phpmyadmin to the apache configuration by executing the following command.


```sh
$ sudo nano /etc/apache2/apache2.conf
```
This will bring up the apache2 config file, use ‘CTRL’ + ‘V’ on mac to scroll to the bottom of the file

# Include web access to phpmyadmin
Include /etc/phpmyadmin/apache.conf

```sh
$ sudo service apache2 restart
```

### Install composer on Amazon AMI running on EC2
```sh
$ cd ~
$ sudo curl -sS https://getcomposer.org/installer | sudo php
$ sudo mv composer.phar /usr/local/bin/composer
$ sudo ln -s /usr/local/bin/composer /usr/bin/composer
```

## Setup GIT for web deployment and version control
sudo apt-get install git

